<?php
include_once("IModel.php");
include_once("Book.php");
include_once("TestDBProps.php");

/** The Model is the class holding data about a collection of books. 
 * @author Rune Hjelsvold
 * @see http://php-html.net/tutorials/model-view-controller-in-php/ The tutorial code used as basis.
 */
class DBModel implements IModel
{        
    /**
      * The PDO object for interfacing the database
      *
      */
    protected $db = null;  
    
    /**
	 * @throws PDOException
     */
    public function __construct($db = null)  
    {  
	    if ($db) 
		{
			$this->db = $db;
		}
		else
		{
            // Create PDO connection
			$this->db = new PDO('mysql:host='.TEST_DB_HOST.'; dbname=books; charset=utf8', 
			TEST_DB_USER, TEST_DB_PWD);
			$this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		}
    }
    
    /** Function returning the complete list of books in the collection. Books are
     * returned in order of id.
     * @return Book[] An array of book objects indexed and ordered by their id.
	 * @throws PDOException
     */
    public function getBookList()
    {
		$booklist = array();
		$stmt = $this->db->prepare("SELECT id, 
									title, 
									author, 
									description 
									FROM book"); 
		$stmt->execute();
		$booklist = $stmt->fetchAll(PDO::FETCH_OBJ);
        return $booklist;
    }
    
    /** Function retrieving information about a given book in the collection.
     * @param integer $id the id of the book to be retrieved
     * @return Book|null The book matching the $id exists in the collection; null otherwise.
	 * @throws PDOException
     */
    public function getBookById($id)
    {
		$book = null;
		if(filter_var($id, FILTER_VALIDATE_INT) !== false){
		$stmt = $this->db->prepare("SELECT * FROM book WHERE id = :id");
		$stmt->bindParam(":id", $id);
		$stmt->execute();
		$res = $stmt->fetch(PDO::FETCH_ASSOC);
		if($res){
		$book = new Book($res["title"], 
						$res["author"], 
						$res["description"], 
						$res["id"]);
		}
		}else{
			$view = new ErrorView("ID not found!");
			$view->create();
		}
        return $book;
    }
    
    /** Adds a new book to the collection.
     * @param $book Book The book to be added - the id of the book will be set after successful insertion.
	 * @throws PDOException
     */
    public function addBook($book)
    {

		try{
		$stmt = $this->db->prepare("INSERT INTO book(title, author, description)
									VALUES(:title, :author, :description)");
		$stmt->bindValue(':title', $book->title, PDO::PARAM_STR);
		$stmt->bindValue(':author', $book->author, PDO::PARAM_STR);
		$stmt->bindValue(':description', $book->description, PDO::PARAM_STR);
		if(empty($book->title) || empty($book->author)){
			$view = new ErrorView("Failed to add title and/or author: ");
			$view->create();
		}else{
			if(is_int($book->id)){
			$stmt->execute();
			$book->id=$this->db->lastInsertId();
			}else{
			$view = new ErrorView("Failed to add book: ");
			$view->create();	
			}
		}
		}
	catch(PDOException $e){
		$view = new ErrorView("Failed to add book: " . $e->getMessage());
		$view->create();
		}
		
	}
	

    /** Modifies data related to a book in the collection.
     * @param $book Book The book data to be kept.
     * @todo Implement function using PDO and a real database.
     */
    public function modifyBook($book)
    {
		if(!($book->id >= 0) || !is_numeric($book->id) || $book->id == null){
		$view = new ErrorView("Failed to modify book: ");
		$view->create();
		}else{
		try{
		$stmt = $this->db->prepare("UPDATE book SET title=:title, 
									author=:author, 
									description=:description 
									WHERE id=:id");
		$stmt->bindValue(':title', $book->title, PDO::PARAM_STR);
		$stmt->bindValue(':author', $book->author, PDO::PARAM_STR);
		$stmt->bindValue(':description', $book->description, PDO::PARAM_STR);
		$stmt->bindValue(':id', $book->id);
		if(empty($book->title) || empty($book->author)){
			$view = new ErrorView("Failed to modify title and/or author: ");
			$view->create();
		}else{
		$stmt->execute();
		}
		}
		catch(PDOException $e){
		$view = new ErrorView("Failed to modify book: " . $e->getMessage());
		$view->create();
		}
		}
    }

    /** Deletes data related to a book from the collection.
     * @param $id integer The id of the book that should be removed from the collection.
     */
    public function deleteBook($id)
    {
		try{
		$delete = $this->db->prepare("DELETE FROM book WHERE id=$id");
		$delete->execute();	
		}
		catch(PDOException $e){
		$view = new errorView("Failed to delete book: ". $e->getMessage());
		$view->create();
		}
    }
	
}

?>